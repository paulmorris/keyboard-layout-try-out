# keyboard-layout-try-out

A tool for trying out different keyboard layouts using existing muscle memory.

Converts a set of words so that typing them in your current layout uses the same
movements as typing them in a different layout.
