import { refreshUrlParamsFromLayouts, cloneTemplate } from "./index.js";

customElements.define(
  "layout-select",
  class extends HTMLElement {
    constructor() {
      super();

      const select = cloneTemplate("#layoutSelect");
      if (!select) return;

      select.addEventListener("input", (event) => {
        // @ts-expect-error
        const value = event.target?.value;

        /** @type {HTMLTextAreaElement | null | undefined} */
        const layoutTextInput =
          this.closest(".layoutInputBox")?.querySelector(".layoutTextInput");

        if (value && layoutTextInput) {
          layoutTextInput.value = value;

          refreshUrlParamsFromLayouts();
        }
      });

      this.appendChild(select);
    }
  }
);
