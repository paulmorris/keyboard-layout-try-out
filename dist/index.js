// @ts-check

/**
 * @typedef {string[][][]} Layout
 * layout[row][column][layer]
 * layer 0 is base characters (unshifted), layer 1 is shifted characters
 */

const fallbackChar = "□";

/** @type {(layoutNumber: number | string, rowNumber: number) => string} */
const makeParamPrefix = (layoutNumber, rowNumber) =>
  `l${layoutNumber}r${rowNumber}`;

window.addEventListener("load", () => {
  restoreLayoutsFromUrlParams();
  refreshLayoutHeadings();

  document
    .querySelector("#convertWordsButton")
    ?.addEventListener("click", handleConvertWords);

  document
    .querySelector("#addExtraLayoutButton")
    ?.addEventListener("click", () => addAnotherLayout());

  /** @type {HTMLTextAreaElement | null} */
  const elemLayout0 = document.querySelector("#layout0");

  elemLayout0?.addEventListener("input", refreshUrlParamsFromLayouts);

  document
    .querySelector("#layout1")
    ?.addEventListener("input", refreshUrlParamsFromLayouts);

  document
    .querySelector("#firstRemoveLayoutButton")
    ?.addEventListener("click", removeLayoutEventListener);

  if (elemLayout0 && elemLayout0.value.trim().length === 0) {
    elemLayout0.value =
      "q w e r t  y u i o p\na s d f g  h j k l ; '\nz x c v b  n m , . /";
  }

  // Set up word loading buttons.

  document
    .querySelector("#wordsInSelect")
    ?.addEventListener("input", async (event) => {
      // @ts-expect-error
      const value = event.target?.value;

      if (value) {
        const modulePath = `./${value}.js`;

        const module = await import(modulePath);

        /** @type {HTMLTextAreaElement | null} */
        const wordsIn = document.querySelector("#wordsIn");

        if (wordsIn) wordsIn.value = module.words;

        refreshUrlParamsFromLayouts();
      }
    });

  // Set up typing feature.

  /** @type {HTMLInputElement | null} */
  const typingInput = document.querySelector("#typingInput");
  const typingOverlay = document.querySelector("#typingOverlay");
  const typeWordsButton = document.querySelector("#typeWordsButton");

  typeWordsButton?.addEventListener("click", () => {
    typingOverlay?.classList.add("displayFlex");
    if (typingInput) typingInput.value = "";
    typingInput?.focus();
  });
  typeWordsButton?.setAttribute("disabled", "");
  typeWordsButton?.classList.remove("displayNone");

  typingOverlay?.addEventListener("click", () => {
    typingInput?.focus();
  });

  document.querySelector("#exitTypingButton")?.addEventListener("click", () => {
    typingOverlay?.classList.remove("displayFlex");
  });

  document
    .querySelector("#restartTypingButton")
    ?.addEventListener("click", () => {
      handleConvertWords();
    });
});

export function refreshUrlParamsFromLayouts() {
  /** @type {NodeListOf<HTMLTextAreaElement>} */
  const layoutInputElements = document.querySelectorAll(
    ".layoutInputContainer .layoutTextInput"
  );

  const layouts = [...layoutInputElements].map((elem) =>
    elem.value.split("\n")
  );

  const params = new URLSearchParams();

  layouts.forEach((layout, layoutIndex) => {
    layout.forEach((row, rowIndex) => {
      if (row.length > 0) {
        params.set(makeParamPrefix(layoutIndex, rowIndex), row);
      }
    });
  });

  // Add a final unused param to prevent link formatting from leaving out
  // final punctuation character like period. (Post URL online with
  // punctuation as final character -> final character is left out of the link.)
  params.set("z", "z");

  const paramsString = params.toString();

  const url = paramsString.length
    ? `${window.location.pathname}?${paramsString}`
    : window.location.pathname;

  history.replaceState(null, "", url);
}

function restoreLayoutsFromUrlParams() {
  const params = new URLSearchParams(window.location.search);
  if (params.size === 0) {
    return;
  }

  const layoutParamRegex = /^l([0-9]+)r/;

  const layoutNumbersSet = new Set(
    [...params.keys()]
      .map((s) => s.match(layoutParamRegex)?.[1])
      .filter((k) => k !== undefined)
  );

  const layoutNumbers = [...layoutNumbersSet].sort();

  /** @type {NodeListOf<HTMLTextAreaElement>} */
  const layoutInputElements = document.querySelectorAll(
    ".layoutInputContainer .layoutTextInput"
  );

  layoutNumbers.forEach((layoutNumber) => {
    const layoutValue = layoutFromUrlParams(params, layoutNumber);
    const input = layoutInputElements[layoutNumber];

    if (input) {
      input.value = layoutValue;
    } else {
      addAnotherLayout(layoutValue);
    }
  });
}

/**
 * @param {URLSearchParams} params
 * @param {string} layoutNumber
 */
function layoutFromUrlParams(params, layoutNumber) {
  const layout = [];

  // Count down to support having empty rows between non-empty rows.
  for (const rowNumber of [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) {
    const row = params.get(makeParamPrefix(layoutNumber, rowNumber));

    if (row || layout.length > 0) {
      layout.push(`${row || ""}\n`);
    }
  }
  return layout.reverse().join("");
}

/**
 * @param {Event} event
 */
function removeLayoutEventListener(event) {
  // @ts-expect-error
  event.target?.closest(".layoutInputBox").remove();
  refreshUrlParamsFromLayouts();
  refreshLayoutHeadings();
}

/** @type {(layoutValue?: string) => void} */
function addAnotherLayout(layoutValue) {
  const clone = cloneTemplate("#extraLayoutTemplate");
  if (!clone) return;

  clone
    .querySelector(".removeLayoutButton")
    ?.addEventListener("click", removeLayoutEventListener);

  const layoutInputContainer = document.querySelector(".layoutInputContainer");
  if (!layoutInputContainer) return;

  /** @type {HTMLTextAreaElement | null} */
  const layoutTextInput = clone.querySelector(".layoutTextInput");

  layoutTextInput?.addEventListener("input", refreshUrlParamsFromLayouts);

  if (layoutValue && layoutTextInput) {
    layoutTextInput.value = layoutValue;
  }

  layoutInputContainer.appendChild(clone);
  refreshLayoutHeadings();
}

function refreshLayoutHeadings() {
  const layoutToTryHeadings = document.querySelectorAll(
    ".layoutInputContainer .layoutToTryHeading"
  );
  const first = layoutToTryHeadings[0];

  if (layoutToTryHeadings.length === 1 && first) {
    first.textContent = "Layout To Try Out";
  } else {
    [...layoutToTryHeadings].forEach((heading, index) => {
      heading.textContent = `Layout To Try Out ${index + 1}`;
    });
  }
}

function handleConvertWords() {
  /** @type {HTMLTextAreaElement | null} */
  const elemWordsIn = document.querySelector("#wordsIn");

  /** @type {HTMLTextAreaElement | null} */
  const elemWordsOut = document.querySelector("#wordsOut");

  /** @type {HTMLTextAreaElement | null} */
  const elemLayout0 = document.querySelector("#layout0");

  /** @type {NodeListOf<HTMLTextAreaElement>} */
  const layoutInputElements = document.querySelectorAll(
    ".layoutInputContainer .layoutTextInput"
  );

  const wordsIn = (elemWordsIn?.value || "").trim().split(/\s+/);

  const layoutsToTry = [];

  layoutInputElements?.forEach((input, index) => {
    if (index !== 0) {
      layoutsToTry.push(input?.value || "");
    }
  });

  const { wordPairGroups, layoutWarnings } = convertWords(
    elemLayout0?.value || "",
    layoutsToTry,
    wordsIn
  );

  setLayoutWarnings(layoutWarnings);

  const wordPairGroupsAfterOmit = maybeOmitIdenticalWords(wordPairGroups);

  const stringToType = setUpTyping(wordPairGroupsAfterOmit);

  if (elemWordsOut) {
    elemWordsOut.value = stringToType;
  }
}

/**
 * @param {string} inputLayout0
 * @param {string[]} inputLayouts
 * @param {string[]} wordsIn
 * @returns {{wordPairGroups: string[][][], layoutWarnings: (string | undefined)[]}}
 */
function convertWords(inputLayout0, inputLayouts, wordsIn) {
  const layout0 = parseLayoutInput(inputLayout0);

  const results = inputLayouts.map((inputLayoutN) => {
    const layoutN = parseLayoutInput(inputLayoutN);
    return convertWordsForLayout(layout0, layoutN, wordsIn);
  });

  /** @type {string[][][]} */
  const wordPairGroups = [];

  wordsIn.forEach((_, index) => {
    const group = results.map((r) => r.wordPairs[index]);
    wordPairGroups.push(group);
  });

  return {
    wordPairGroups,
    layoutWarnings: results.map((r) => r.layoutWarnings),
  };
}

/**
 * @param {Layout} layout0
 * @param {Layout} layoutN
 * @param {string[]} wordsIn
 * @returns {{wordPairs: string[][], layoutWarnings: string | undefined}}
 */
function convertWordsForLayout(layout0, layoutN, wordsIn) {
  const characterMap = makeCharacterToCharacterMap(layout0, layoutN);

  const unconvertedChars = new Set();
  const passthroughChars = new Set();

  const wordPairs = wordsIn.map((word) => {
    const convertedChars = [];

    for (const char of word) {
      const convertedChar = characterMap.get(char);

      if (!convertedChar) {
        passthroughChars.add(char);
      } else if (convertedChar === fallbackChar) {
        unconvertedChars.add(char);
      }
      convertedChars.push(convertedChar || char);
    }

    return [word, convertedChars.join("")];
  });

  return {
    wordPairs,
    layoutWarnings: makeLayoutWarnings(
      layout0,
      layoutN,
      characterMap,
      unconvertedChars,
      passthroughChars
    ),
  };
}

/**
 * @param {string[][][]} wordPairGroups
 * @returns {string[][][]}
 */
function maybeOmitIdenticalWords(wordPairGroups) {
  /** @type {HTMLSelectElement | null} */
  const omitWordsSelect = document.querySelector("#omitWordsSelect");

  if (omitWordsSelect?.value === "omitSameInCurrentAndTryOutLayouts") {
    // Omit words that are not the same in all layouts.
    return wordPairGroups.filter((group) => {
      return group.some((pair) => pair[0] !== pair[1]);
    });
  }
  if (omitWordsSelect?.value === "omitSameInTryOutLayouts") {
    // Omit words that are the same in all layouts to try out,
    // but possibly different from the current layout.
    return wordPairGroups.filter((group) => {
      const word = group[0][1];
      return !group.every((pair) => word === pair[1]);
    });
  }
  return wordPairGroups;
}

/**
 * Take layout0 (current) and layout1 (to try) and make a map from characters
 * in layout1 to characters in layout0 that will be used to convert words.
 * Get the positions of all characters in layout1 and then find the characters
 * at those positions in layout0, then map the characters.
 *
 * [layout1 chars] -> [layout1 posns] -> [layout0 chars (at those posns)]
 *
 * @param {Layout} layout0
 * @param {Layout} layout1
 * @returns {Map<string, string>}
 */
function makeCharacterToCharacterMap(layout0, layout1) {
  const map = new Map([
    [" ", " "],
    ["\n", "\n"],
  ]);

  const charToPosition1 = makeCharacterToPositionMap(layout1);

  for (const position of charToPosition1.entries()) {
    const [charInLayout1, { row, col, layer }] = position;

    /** @type {string | undefined} */
    const convertedCharInLayout0 = layout0[row]?.[col]?.[layer];

    map.set(charInLayout1, convertedCharInLayout0 || fallbackChar);
  }

  // Show fallback for characters in layout0 but missing from layout1.
  // Otherwise the characters are silently not converted.
  layout0.flat(2).forEach((char) => {
    if (!map.has(char)) {
      map.set(char, fallbackChar);
    }
  });

  return map;
}

/**
 * @param {string} layoutString
 * @param {Map<string, string>} shiftPairs
 * @returns {Layout}
 */
function parseLayoutInput(
  layoutString,
  shiftPairs = unshiftedToShiftedSymbols
) {
  const rows = layoutString.split("\n");

  const parsed = rows.map((row) =>
    row
      .split(/\s*/)
      .filter((ch) => ch)
      .map((char) => {
        // Gracefully handle uppercase alphas in layout input.
        if (upperAlphasSet.has(char)) {
          return [char.toLowerCase(), char];
        }

        const shiftedChar = shiftPairs.get(char);
        return shiftedChar ? [char, shiftedChar] : [char];
      })
  );

  return parsed;
}

/**
 * @param {Layout} layout
 */
function makeCharacterToPositionMap(layout) {
  /** @type {Map<string, {row: number, col: number, layer: number}>} */
  const map = new Map();

  /** @param {number} layer */
  const mapCharsInLayer = (layer) => {
    layout.forEach((row, rowIndex) => {
      row.forEach((col, colIndex) => {
        const char = col[layer];
        if (char) {
          map.set(char, { row: rowIndex, col: colIndex, layer });
        }
      });
    });
  };

  // Do two passes, first for the base layer characters and then for shifted
  // characters, because if a character is on the base layer it has
  // priority over the same character in shifted layer.
  mapCharsInLayer(0);
  mapCharsInLayer(1);

  return map;
}

/**
 * @param {(string | undefined)[]} layoutWarnings
 */
function setLayoutWarnings(layoutWarnings) {
  const layoutWarningsContainer = document.querySelector(
    "#layoutWarningsContainer"
  );
  if (!layoutWarningsContainer) return;

  while (layoutWarningsContainer.firstChild) {
    layoutWarningsContainer.firstChild.remove();
  }

  layoutWarnings.forEach((warning, index) => {
    if (warning) {
      const p = document.createElement("p");
      p.setAttribute("class", "layoutWarnings");

      p.innerText =
        layoutWarnings.length > 1
          ? `[Layout ${index + 1} Warnings] ${warning}`
          : `[Warnings] ${warning}`;

      layoutWarningsContainer.appendChild(p);
    }
  });
}

/**
 * @param {Layout} layout0
 * @param {Layout} layout1
 * @param {Map<string, string>} characterMap
 * @param {Set<string>} unconvertedChars
 * @param {Set<string>} passthroughChars
 * @returns {string | undefined}
 */
function makeLayoutWarnings(
  layout0,
  layout1,
  characterMap,
  unconvertedChars,
  passthroughChars
) {
  const unconvertedCharsWarning =
    unconvertedChars.size > 0
      ? `Unable to convert: ${[...unconvertedChars].join(" ")}`
      : undefined;

  const passthroughCharsWarning =
    passthroughChars.size > 0
      ? `Passed through without being converted: ${[...passthroughChars].join(
          " "
        )}`
      : undefined;

  /** @type {HTMLSelectElement | null} */
  const layoutWarningsSelect = document.querySelector("#layoutWarningsSelect");

  const showAllWarnings = layoutWarningsSelect?.value === "all";

  const joinedWarnings = [
    unconvertedCharsWarning,
    passthroughCharsWarning,
    ...(showAllWarnings
      ? generateMoreWarnings(layout0, layout1, unconvertedChars, characterMap)
      : []),
  ]
    .filter((s) => s)
    .join("] [");

  return joinedWarnings.length > 0 ? `[${joinedWarnings}]` : undefined;
}

/**
 * @param {Layout} layout0
 * @param {Layout} layout1
 * @param {Set<string>} unconvertedChars
 * @param {Map<string, string>} characterMap
 */
function generateMoreWarnings(
  layout0,
  layout1,
  unconvertedChars,
  characterMap
) {
  const flatLayout0 = layout0.flat(2);
  const flatLayout1 = layout1.flat(2);

  const unconvertableChars = findNoMatchChars(characterMap);

  const missingLayout0 = findMissingChars(flatLayout0);
  const missingLayout1 = findMissingChars(flatLayout1);

  const duplicatesLayout0 = findDuplicateChars(flatLayout0);
  const duplicatesLayout1 = findDuplicateChars(flatLayout1);

  const moreUnconvertibleChars = [...unconvertableChars].filter(
    (c) => !unconvertedChars.has(c)
  );

  const couldNotConvertWarning =
    moreUnconvertibleChars.length > 0
      ? `Unconvertible: ${moreUnconvertibleChars.join(" ")}`
      : undefined;

  const missingWarningLayout0 =
    missingLayout0.size > 0
      ? `Current layout is missing: ${[...missingLayout0].join(" ")}`
      : undefined;

  const missingWarningLayout1 =
    missingLayout1.size > 0
      ? `Layout to try out is missing: ${[...missingLayout1].join(" ")}`
      : undefined;

  const duplicatesWarningLayout0 =
    duplicatesLayout0.size > 0
      ? `Current layout has duplicates: ${[...duplicatesLayout0].join(" ")}`
      : undefined;

  const duplicatesWarningLayout1 =
    duplicatesLayout1.size > 0
      ? `Layout to try out has duplicates: ${[...duplicatesLayout1].join(" ")}`
      : undefined;

  return [
    couldNotConvertWarning,
    missingWarningLayout0,
    missingWarningLayout1,
    duplicatesWarningLayout0,
    duplicatesWarningLayout1,
  ];
}

/**
 * @param {string[]} chars
 */
function findMissingChars(chars) {
  const charsSet = new Set(chars);

  /** @type {Set<string>} */
  const missing = new Set();

  primaryUnshiftedChars.forEach((char) => {
    if (!charsSet.has(char)) {
      missing.add(char);
    }
  });
  return missing;
}

/**
 * @param {string[]} chars
 */
function findDuplicateChars(chars) {
  /** @type {Set<string>} */
  const duplicates = new Set();

  [...chars].sort().forEach((char, index, sortedChars) => {
    if (char === sortedChars[index + 1]) {
      duplicates.add(char);
    }
  });

  return duplicates;
}

/**
 * @param {Map<string, string>} characterMap
 */
function findNoMatchChars(characterMap) {
  /** @type {Set<string>} */
  const missing = new Set();

  characterMap.forEach((value, key) => {
    if (value === fallbackChar) {
      missing.add(key);
    }
  });

  return missing;
}

const alphas = /** @type {const} */ ([
  ["a", "A"],
  ["b", "B"],
  ["c", "C"],
  ["d", "D"],
  ["e", "E"],
  ["f", "F"],
  ["g", "G"],
  ["h", "H"],
  ["i", "I"],
  ["j", "J"],
  ["k", "K"],
  ["l", "L"],
  ["m", "M"],
  ["n", "N"],
  ["o", "O"],
  ["p", "P"],
  ["q", "Q"],
  ["r", "R"],
  ["s", "S"],
  ["t", "T"],
  ["u", "U"],
  ["v", "V"],
  ["w", "W"],
  ["x", "X"],
  ["y", "Y"],
  ["z", "Z"],
]);

const primaryPunctuation = /** @type {const} */ ([
  [",", "<"],
  [".", ">"],
  ["/", "?"],
  [";", ":"],
  ["'", '"'],
]);

const secondaryPunctuation = /** @type {const} */ ([
  ["`", "~"],
  ["1", "!"],
  ["2", "@"],
  ["3", "#"],
  ["4", "$"],
  ["5", "%"],
  ["6", "^"],
  ["7", "&"],
  ["8", "*"],
  ["9", "("],
  ["0", ")"],
  ["-", "_"],
  ["=", "+"],
  ["[", "{"],
  ["]", "}"],
  ["\\", "|"],
]);

const primaryUnshiftedChars = [
  ...alphas.map(([lower]) => lower),
  ...primaryPunctuation.map(([lower]) => lower),
];

/** @type {Set<string>} */
const upperAlphasSet = new Set(alphas.map(([lower, upper]) => upper));

const unshiftedToShiftedSymbols = new Map([
  ...alphas,
  ...primaryPunctuation,
  ...secondaryPunctuation,
]);

/**
 * @param {string[][][]} wordPairGroups
 * @returns {string}
 */
function setUpTyping(wordPairGroups) {
  /** @type {HTMLSelectElement | null} */
  const wordOrderSelect = document.querySelector("#wordOrderSelect");

  if (wordOrderSelect?.value === "shuffle") {
    shuffleWordPairGroups(wordPairGroups);
  }

  const wordGroupsReal = wordPairGroups.map((group) =>
    group.map((pair) => pair[0])
  );
  const wordGroupsToType = wordPairGroups.map((group) =>
    group.map((pair) => pair[1])
  );

  const maxRowSize = 55;
  const splitRealWords = wordGroupsToLetterRows(wordGroupsReal, maxRowSize);
  const splitWordsToType = wordGroupsToLetterRows(wordGroupsToType, maxRowSize);

  /** @type {HTMLDivElement | null} */
  const typingRows = document.querySelector("#typingRows");
  if (!typingRows) return "";

  // Remove any existing row elements.
  while (typingRows?.firstChild) {
    typingRows.removeChild(typingRows.firstChild);
  }

  // Add first two rows to the DOM to start with.
  splitRealWords[0] &&
    splitWordsToType[0] &&
    addRowToDom(splitRealWords[0], splitWordsToType[0]);

  splitRealWords[1] &&
    splitWordsToType[1] &&
    addRowToDom(splitRealWords[1], splitWordsToType[1]);

  // Set up input listener.
  /** @type {HTMLInputElement | null} */
  const typingInput = document.querySelector("#typingInput");

  typingInput?.removeEventListener("keydown", handleKeyDown);
  handleKeyDown = makeKeyDownHandler(
    splitRealWords,
    splitWordsToType,
    typingRows
  );
  typingInput?.addEventListener("keydown", handleKeyDown);

  typingRows?.firstElementChild?.classList.remove("displayNone");

  if (wordGroupsToType.length > 0) {
    document.querySelector("#typeWordsButton")?.removeAttribute("disabled");
  }

  return wordGroupsToType.flat().join(" ");
}

/**
 * @param {string[]} realLetters
 * @param {string[]} lettersToType
 */
function addRowToDom(realLetters, lettersToType) {
  /** @type {HTMLDivElement | null} */
  const typingRows = document.querySelector("#typingRows");
  if (!typingRows) return;

  const row = cloneTemplate("#typingRowTemplate");
  if (!row) return;

  const wordsToTypeRow = row.querySelector(".wordsToTypeRow");
  const realWordsRow = row.querySelector(".realWordsRow");
  if (!wordsToTypeRow || !realWordsRow) return;

  const lettersToTypeElems = makeArrayOfLetterElems(lettersToType);
  const realLettersElems = makeArrayOfLetterElems(realLetters);

  wordsToTypeRow.append(...lettersToTypeElems);
  realWordsRow.append(...realLettersElems);

  typingRows.append(row);
}

/**
 * Randomize array in-place using Durstenfeld shuffle algorithm
 *
 * @param {string[][][]} array
 */
function shuffleWordPairGroups(array) {
  for (let index = array.length - 1; index >= 0; index--) {
    const randomIndex = Math.floor(Math.random() * (index + 1));

    const temp = array[index];
    array[index] = array[randomIndex];
    array[randomIndex] = temp;
  }
}

/**
 * @param {string[][]} wordGroupsArray
 * @param {number} maxRowSize
 */
function wordGroupsToLetterRows(wordGroupsArray, maxRowSize) {
  // Make a copy that can be destroyed.
  const wordGroups = [...wordGroupsArray];

  /** @type {string[][]} */
  const letterRows = [];

  while (wordGroups.length > 0) {
    /** @type {string[]} */
    const letters = [];

    // Always add at least one word group to handle the case where the group is
    // larger than the max row size.
    // TODO: do we break up the word group if the group is larger than max row
    // size?
    while (
      wordGroups.length > 0 &&
      (letters.length === 0 ||
        letters.length + calculateWordGroupLength(wordGroups[0]) < maxRowSize)
    ) {
      const wordGroup = wordGroups.shift();
      if (wordGroup) {
        letters.push(...wordGroup.join(" "), " ");
      }
    }

    letterRows.push(letters);
  }
  return letterRows;
}

/**
 * @param {string[]} wordGroup
 */
function calculateWordGroupLength(wordGroup) {
  return wordGroup.reduce((total, word) => total + word.length + 1, 0);
}

/**
 * @param {string[]} row
 * @returns {HTMLSpanElement[]}
 */
function makeArrayOfLetterElems(row) {
  return row.map((letter) => {
    const span = document.createElement("span");
    span.appendChild(document.createTextNode(letter));
    return span;
  });
}

const ignoredKeys = new Set([
  "Tab",
  "Shift",
  "Enter",
  "Escape",
  "CapsLock",
  "Control",
  "Alt",
  "Delete",
  "End",
  "Home",
  "ArrowLeft",
  "ArrowRight",
  "ArrowUp",
  "ArrowDown",
  "PageUp",
  "PageDown",
  "F1",
  "F2",
  "F3",
  "F4",
  "F5",
  "F6",
  "F7",
  "F8",
  "F9",
  "F10",
  "F11",
  "F12",
  "F13",
]);

/**
 * @param {KeyboardEvent} event
 */
let handleKeyDown = (event) => {};

/**
 * @param {string[][]} realLetters
 * @param {string[][]} lettersToType
 * @param {HTMLDivElement} typingRows
 */
function makeKeyDownHandler(realLetters, lettersToType, typingRows) {
  // The position of the cursor within the current row.
  let cursorPosition = 0;
  let rowNumber = 0;

  /**
   * @param {KeyboardEvent} event
   */
  return (event) => {
    if (ignoredKeys.has(event.key)) return;

    if (event.key === "Backspace") {
      if (rowNumber === 0 && cursorPosition === 0) {
        // We are at the very beginning, do nothing.
        return;
      }

      rowNumber = cursorPosition === 0 ? rowNumber - 1 : rowNumber;

      const toTypeRow = typingRows.children[rowNumber].firstElementChild;
      if (!toTypeRow) return;

      cursorPosition =
        cursorPosition === 0
          ? toTypeRow.childElementCount - 1
          : cursorPosition - 1;

      const toTypeLetter = toTypeRow?.children[cursorPosition];
      const realLetter =
        toTypeRow?.nextElementSibling?.children[cursorPosition];

      if (toTypeLetter && realLetter) {
        toTypeLetter.className = "";
        realLetter.className = "";

        if (!toTypeLetter.nextElementSibling) {
          // We have moved to the end of the previous row, so show that row.
          const thisRowElem = toTypeLetter.parentElement?.parentElement;
          const nextRowElem = thisRowElem?.nextElementSibling;

          thisRowElem?.classList.remove("displayNone");
          nextRowElem?.classList.add("displayNone");
        }
      }

      return;
    }

    // Is not backspace.

    // We were at the end of the row before the key was pressed, so we need to
    // move to the next row.
    const needNextRow = cursorPosition === lettersToType[rowNumber].length - 1;

    if (rowNumber === lettersToType.length - 1 && needNextRow) {
      // We are at the very end, do nothing.
      return;
    }

    const toTypeRow = typingRows.children[rowNumber].firstElementChild;

    const toTypeLetter = toTypeRow?.children[cursorPosition];
    const realLetter = toTypeRow?.nextElementSibling?.children[cursorPosition];

    const isCorrect = toTypeLetter?.textContent === event.key;

    if (toTypeLetter && realLetter) {
      const className = isCorrect ? "correct" : "incorrect";
      toTypeLetter.className = className;
      realLetter.className = className;
    }

    if (toTypeLetter && !toTypeLetter.nextElementSibling) {
      // We are at the end of the row, show the next row.
      const thisRow = toTypeLetter.parentElement?.parentElement;
      const nextRow = thisRow?.nextElementSibling;

      thisRow?.classList.add("displayNone");
      nextRow?.classList.remove("displayNone");

      if (!nextRow?.nextElementSibling) {
        // Add another row to the DOM, if needed.
        const lettersToTypeToAdd = lettersToType[rowNumber + 2];
        const realLettersToAdd = realLetters[rowNumber + 2];

        if (lettersToTypeToAdd && realLettersToAdd) {
          addRowToDom(realLettersToAdd, lettersToTypeToAdd);
        }
      }
    }

    rowNumber = needNextRow ? rowNumber + 1 : rowNumber;
    cursorPosition = needNextRow ? 0 : cursorPosition + 1;
  };
}

/**
 * @param {string} templateSelector
 * @returns {Element | null}
 */
export function cloneTemplate(templateSelector) {
  /** @type {HTMLTemplateElement | null} */
  const template = document.querySelector(templateSelector);
  if (!template) return null;

  const clone = /** @type {DocumentFragment} */ (
    template.content.cloneNode(true)
  );

  const element = clone.firstElementChild;

  return element;
}
